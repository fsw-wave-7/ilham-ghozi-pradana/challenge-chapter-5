const express = require('express');
const app = express();
const router = express.Router();

// ==================== ROUTING =======================
router.get('/', (req, res) => {
    // res.sendFile(path.join(__dirname + '/index.html'));
    res.render('home');
});

router.get('/game', (req, res) => {
    // res.sendFile(path.join(__dirname + '/public/game.html'));
    res.render('game');
});

router.get('/signup', (req, res) => {
    res.render('signup');
})

router.get('/login', (req, res) => {
    res.render('login');
})

router.post('/signup', (req, res) => {
    //destructuring the req.body
    const { email, password } = req.body;

    // get the latest ID
    const id = userDb[userDb.length - 1].id + 1;

    //preparing the data
    const post = {
        id,
        email,
        password
    }
    //push the data to the file
    userDb.push(post);

    // write the data to file user.json
    fs.writeFile('./masterdata/user.json', JSON.stringify(userDb), (error) => {
        if (error) {
            res.status(400).json({
                message: "error pushing the data to the database"
            })
        }
        res.status(200).redirect('/');
    })
});
