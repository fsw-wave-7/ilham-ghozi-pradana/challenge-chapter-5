const express = require('express');
const api = express();
const fs = require('fs');
const userDb = require('./masterdata/user.json');


// ================ RESTful API ==================
api.get('/api/user', (req, res) => {
    res.status(200).json(userDb);
})
api.get('/api/user/:id', (req, res) => {
    const post = userDb.find( x => x.id === parseInt(req.params.id))
    if (!post) {
        res.status(200).json({
            status: "error",
            message: "id not found"
        })
    }
    else res.status(200).json(post)
})
api.post('/api/user', (req, res) => {
    //destructuring the req.body
    const { email, password } = req.body;

    // get the latest ID
    const id = userDb[userDb.length - 1].id + 1;

    //preparing the data
    const post = {
        id,
        email,
        password
    }
    //push the data to the file
    userDb.push(post);

    // write the data to file user.json
    fs.writeFile('./masterdata/user.json', JSON.stringify(userDb), (error) => {
        if (error) {
            res.status(400).json({
                message: "error pushing the data to the database"
            })
        }
        res.status(200).json(post);
    })
});
api.put('/api/user/:id', (req,res) => {
    const post = userDb.find( x => x.id === parseInt(req.params.id, 10))
    // console.log(post)

    if (!post) {
        res.status(200).json({
            status: "error",
            message: "id not found"
        })
    }
    else {
    const params = {
        email: req.body.email,
        password: req.body.password
    }
    // console.log(params)
    updatedPost = {
        ...post,
        ...params
    }
    // console.log(updatedPost)
    userDb = userDb.map(x => x.id === updatedPost.id ? updatedPost: x)

    // write the data to file user.json
     fs.writeFile('./masterdata/user.json', JSON.stringify(userDb), (error) => {
        if (error) {
            res.status(400).json({
                message: "error pushing the data to the database"
            })
        }
        res.status(200).json(updatedPost)
    })
}
})
api.delete('/api/user/:id', (req,res) => {
    userDb = userDb.filter( x => x.id !== parseInt(req.params.id, 10))

    // write the data to file user.json
    fs.writeFile('./masterdata/user.json', JSON.stringify(userDb), (error) => {
        if (error) {
            res.status(400).json({
                message: "error pushing the data to the database"
            })
        }
        res.status(200).json({
            message: "deleted",
            userDb
        })
    })
    
})